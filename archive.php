<?php get_header(); ?>

<h1> Archiwum</h1>
<?php if(have_posts()):

		while(have_posts()): the_post(); ?>
		<div class="archive__page">
		<?php get_template_part('content','archive') ?>
	</div>
		<?php endwhile; ?>
		<?php the_posts_navigation();?>
	<?php endif;?>
</div>
	<?php get_footer(); ?>