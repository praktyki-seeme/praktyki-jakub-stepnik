<?php get_header(); ?>

<?php
    $args = array(
        'post_type' => 'movie'
    );

    $post_query = new WP_Query($args);

    if($post_query->have_posts() ) {
        while($post_query->have_posts() ) {
            $post_query->the_post();
            ?>
			<div class="custom-post__all">
			<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
				<?php the_post_thumbnail();?>
			</div>
            <?php
            }
        }
?>
	<?php get_footer(); ?>