<?php
//BOOTSTRAP
///CSS I JS
function wpdocs_theme_name_scripts() {
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js');
	//wp_enqueue_style('bootstrap' , get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css');
	//wp_enqueue_script('bootstrap' , get_stylesheet_directory_uri() . '/bootstrap/js/bootstrap.min.js');
    wp_enqueue_style( 'style.css', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

//IMG W POSTACH
function mytheme_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'mytheme_post_thumbnails' );



/// MENU


function register_custom_nav_menus() {
	register_nav_menus( array(
		'primary' => 'Main',
		'footer_menu' => 'footer',
	) );
	//POSTY
	add_theme_support('post-formats',array('aside','gallery','link'));
}
add_action( 'after_setup_theme', 'register_custom_nav_menus' );
?>
<?php 
function movie_post_type() {
    $labels = array(
        'name'                => 'oferta filmów',
        'singular_name'       => 'Oferta filmów',
        'menu_name'           => 'Oferty filmów',
        'parent_item_colon'   => 'Nadrzędna oferta filmów',
        'all_items'           => 'Wszystkie oferty filmów',
        'view_item'           => 'Zobacz ofertę',
        'add_new_item'        => 'Dodaj ofertę',
        'add_new'             => 'Dodaj nową',
        'edit_item'           => 'Edytuj ofertę',
        'update_item'         => 'Aktualizuj',
        'search_items'        => 'Szukaj ofert filmów',
        'not_found'           => 'Nie znaleziono',
        'not_found_in_trash'  => 'Nie znaleziono'
    ); 
    $args = array(
        'label' => 'movie',
        'rewrite' => array(
            'slug' => 'Filmy'
        ),
        'description'         => 'Filmy',
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true, 
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon'           => 'dashicons-id-alt',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
    );
    register_post_type( 'movie', $args );
} 
add_action( 'init', 'movie_post_type', 0 );
?>

<?php
//Widget
function themename_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'theme_name' ),
        'id'            => 'sidebar-1',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Secondary Sidebar', 'theme_name' ),
        'id'            => 'sidebar-2',
        'before_widget' => '<ul><li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li></ul>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action('widgets_init','themename_widgets_init')

?>