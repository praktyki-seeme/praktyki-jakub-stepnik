<br/>
<article class="content-archive" id="post-<?php the_ID(); ?>" <?php post_class();?>
<header class='content-archive__header'>
<?php the_title(sprintf('<h2><a href="%s">', esc_url(get_permalink() ) )  ,'</a></h2>'); ?>
<small>Opublikowano dnia: <?php the_time('F j, Y');?> </small>
</header>
<div class="size">
<?php if(has_post_thumbnail()):?>
<?php the_post_thumbnail('medium');?>
</div>
<br>
<?php the_content();?>
<?php else: ?>
<?php the_content();?>
<?php endif;?>
</article>