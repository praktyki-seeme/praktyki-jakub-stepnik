<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
  
get_header(); ?>
  <?php 
  $args = array( 'post_type'  => 'post', 
  'posts_per_page' => 1,);
  $the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) : 
    while ( $the_query->have_posts() ) : $the_query->the_post(); 
        the_title( '<h2>', '</h2>' ); 
        the_post_thumbnail(); 
        the_excerpt();
    endwhile; 
else: 
    _e( 'Sorry, no posts matched your criteria.', 'textdomain' ); 
endif; 
?>

 <?php if(get_post_type()=='movie'): ?>
<?php if ( have_posts() ) : ?>
 
    <!-- start of the secondary loop -->
    <?php
        // Start the loop.
        while (have_posts() ) : the_post(); ?>
        
            <div class="custom-post-single">
            <div class="custom-post-single__a">
            <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
				<?php the_post_thumbnail();?>
                <?php the_content();?>
			</div>
            </div>
            <?php
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            
            
            // End the loop.
        endwhile;
        ?>
    <!-- end of the secondary loop -->
 
    <!-- put pagination functions here -->
 
    <!-- reset the main query loop -->
    <?php wp_reset_postdata(); ?>
 
<?php else:  ?>
 
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
 
<?php endif; ?>
<?php endif; ?> 
  

  
<?php get_footer(); ?>